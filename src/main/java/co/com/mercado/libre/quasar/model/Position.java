package co.com.mercado.libre.quasar.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Position {
	private double x;
	private double y;
}
