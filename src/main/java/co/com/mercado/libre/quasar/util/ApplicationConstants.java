package co.com.mercado.libre.quasar.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApplicationConstants {

	public static final String MESSAGE_SEPARATOR = "#";
	public static final String EMPTY_VALUE = "$EMPTY";

	public static final String KENOBI_NAME = "Kenobi";
	public static final double KENOBI_X_POSITION = -500;
	public static final double KENOBI_Y_POSITION = -200;

	public static final String SKYWALKER_NAME = "Skywalker";
	public static final double SKYWALKER_X_POSITION = 100;
	public static final double SKYWALKER_Y_POSITION = -100;

	public static final String SATO_NAME = "Sato";
	public static final double SATO_X_POSITION = 500;
	public static final double SATO_Y_POSITION = 100;
}
